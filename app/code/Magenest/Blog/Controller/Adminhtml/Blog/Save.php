<?php
namespace Magenest\Blog\Controller\Adminhtml\Blog;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Blog\Model\BlogFactory;
use Magenest\Blog\Model\ResourceModel\Blog as BlogResource;
use Magenest\Blog\Model\ResourceModel\Blog\CollectionFactory;

class Save extends Action
{
    protected $blogFactory;
    protected $blogResource;
    protected $blogCollectionFactory;

    public function __construct(
        Context $context,
        BlogFactory $blogFactory,
        BlogResource $blogResource,
        CollectionFactory $blogCollectionFactory
    ) {
        $this->blogFactory = $blogFactory;
        $this->blogResource = $blogResource;
        $this->blogCollectionFactory = $blogCollectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $url = $data['url_rewrite'];
        $blogCollection = $this->blogCollectionFactory->create();
        $allBlogs = $blogCollection->getItems();
        $isDuplicate = false;

        if (!$data) {
            $this->messageManager->addError(__('Invalid data.'));
            return $this->_redirect('*/*/save');
        }

        try {
            $blog = $this->blogFactory->create();

            // Check if the URL already exists
            foreach ($allBlogs as $existingBlog) {
                $existingBlogUrl = $existingBlog->getData('url_rewrite');
                if ($url === $existingBlogUrl && $blog->getId() !== $existingBlog->getId()) {
                    $isDuplicate = true;
                    break;
                }
            }
            if (!$isDuplicate) {
                $blog->setData($data);
                $save= $this->blogResource->save($blog);
                $this->messageManager->addSuccess(__('Blog post has been saved successfully.'));
            } else {
                $this->messageManager->addError(__('The URL is already in use. Please choose a different URL.'));
            }

            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return $this->_redirect('*/*/index');
        }
    }
}
