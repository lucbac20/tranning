<?php
namespace Magenest\Blog\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

class Author implements OptionSourceInterface
{
    /**
     * @var UserCollectionFactory
     */
    protected $userCollectionFactory;

    /**
     * Constructor
     *
     * @param UserCollectionFactory $userCollectionFactory
     */
    public function __construct(UserCollectionFactory $userCollectionFactory)
    {
        $this->userCollectionFactory = $userCollectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $userCollection = $this->userCollectionFactory->create();
        $options = [];

        foreach ($userCollection as $user) {
            $options[] = ['value' => $user->getId(), 'label' => $user->getUsername()];
        }

        return $options;
    }
}
