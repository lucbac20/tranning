<?php

namespace Magenest\Blog\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Status implements OptionSourceInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Get available status options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::STATUS_ENABLED, 'label' => __('Enable')],
            ['value' => self::STATUS_DISABLED, 'label' => __('Disable')]
        ];
    }

    /**
     * Get available status options as key-value pairs
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::STATUS_ENABLED => __('Enable'),
            self::STATUS_DISABLED => __('Disable')
        ];
    }
}
