<?php

namespace Magenest\Blog\Model;

use Magento\Sales\Model\AbstractModel;

class Category extends \Magento\Framework\Model\AbstractModel
{
    public function __construct()
    {
        $this->_init('Magenest\Blog\Model\ResourceModel\Category');
    }
}

