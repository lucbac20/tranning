<?php

namespace Magenest\Blog\Model;

use Magenest\Blog\Api\Data\BlogInterface;

class Blog extends \Magento\Framework\Model\AbstractModel implements BlogInterface
{

    protected function _construct()
    {
        $this->_init('Magenest\Blog\Model\ResourceModel\Blog');
    }

    public function getAuthorId()
    {
        return $this->_getData(BlogInterface::AUTHOR_ID);
    }

    public function setAuthorId($author_id)
    {
        return $this->setData(BlogInterface::AUTHOR_ID, $author_id);
    }

    public function getTitle()
    {
        return $this->_getData(BlogInterface::TITLE);
    }

    public function setTitle($title)
    {
        return $this->setData(BlogInterface::TITLE, $title);
    }

    public function getUrlRewrite()
    {
        return $this->_getData(BlogInterface::URLREWRITE);
    }

    public function setUrlRewrite($url_rewrite)
    {
        return $this->setData(BlogInterface::URLREWRITE, $url_rewrite);
    }

    public function getDescription()
    {
        return $this->_getData(BlogInterface::DESCRIPTION);
    }

    public function setDescription($description)
    {
        return $this->setData(BlogInterface::DESCRIPTION, $description);
    }

    public function getContent()
    {
        return $this->_getData(BlogInterface::CONTENT);
    }

    public function setContent($content)
    {
        return $this->setData(BlogInterface::CONTENT, $content);
    }

    public function getStatus()
    {
        return $this->_getData(BlogInterface::STATUS);
    }

    public function setStatus($status)
    {
        return $this->setData(BlogInterface::STATUS, $status);
    }

    public function getCreatedAt()
    {
        return $this->_getData(BlogInterface::CREATED_AT);
    }

    public function setCreatedAt($created_at)
    {
        return $this->setData(BlogInterface::CREATED_AT, $created_at);
    }

    public function getUpdatedAt()
    {
        return $this->_getData(BlogInterface::UPDATED_AT);
    }

    public function setUpdatedAt($updated_at)
    {
        return $this->setData(BlogInterface::UPDATED_AT, $updated_at);
    }
}
