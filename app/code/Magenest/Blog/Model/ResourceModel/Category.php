<?php

namespace Magenest\Blog\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Category extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_category', 'id'); // 'magenest_category' is the main table name, 'id' is the primary key
    }
}
