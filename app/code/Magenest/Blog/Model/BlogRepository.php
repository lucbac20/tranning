<?php
namespace Magenest\Blog\Model;

use Magenest\Blog\Api\BlogRepositoryInterface;
use Magenest\Blog\Api\Data\BlogInterface;
use Magenest\Blog\Api\Data\BlogInterfaceFactory;
use Magenest\Blog\Model\ResourceModel\Blog\CollectionFactory;
use Magenest\Blog\Model\ResourceModel\Blog as BlogResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class BlogRepository implements BlogRepositoryInterface
{
    /**
     * @var BlogFactory
     */
    private $blogFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var BlogResource
     */
    private $blogResource;

    /**
     * @var Json
     */
    private $jsonSerializer;

    /**
     * @var BlogInterfaceFactory
     */
    private $blogInterfaceFactory;

    /**
     * BlogRepository constructor.
     * @param BlogFactory $blogFactory
     * @param CollectionFactory $collectionFactory
     * @param BlogResource $blogResource
     * @param Json $jsonSerializer
     * @param BlogInterfaceFactory $blogInterfaceFactory
     */
    public function __construct(
        BlogFactory $blogFactory,
        CollectionFactory $collectionFactory,
        BlogResource $blogResource,
        Json $jsonSerializer,
        BlogInterfaceFactory $blogInterfaceFactory
    ) {
        $this->blogFactory = $blogFactory;
        $this->collectionFactory = $collectionFactory;
        $this->blogResource = $blogResource;
        $this->jsonSerializer = $jsonSerializer;
        $this->blogInterfaceFactory = $blogInterfaceFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $blog = $this->blogFactory->create();
        $this->blogResource->load($blog, $id);

        if (!$blog->getId()) {
            throw new NoSuchEntityException(__('Blog post with id "%1" does not exist.', $id));
        }

        return $blog;
    }

    /**
     * Save blog post
     *
     * @param BlogInterface $blog
     * @return BlogInterface
     * @throws CouldNotSaveException
     */
    public function save(BlogInterface $blog)
    {
        try {
            $this->blogResource->save($blog);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not save blog: %1', $e->getMessage()));
        }
        return $blog;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($id)
    {
        $blogModel = $this->blogFactory->create();
        $this->blogResource->load($blogModel, $id);

        if (!$blogModel->getId()) {
            throw new NoSuchEntityException(__('Blog with id "%1" does not exist.', $id));
        }

        try {
            $this->blogResource->delete($blogModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the blog post: %1', $exception->getMessage())
            );
        }

        return true;
    }
}
