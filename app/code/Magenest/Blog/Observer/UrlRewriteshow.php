<?php

namespace Magenest\Blog\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magenest\Blog\Model\Blog; // Thay thế bằng model thực tế của bạn
use Psr\Log\LoggerInterface;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Magento\Framework\App\Cache\TypeListInterface;

class UrlRewriteshow implements ObserverInterface
{
    protected $_urlRewriteFactory;
    protected $blogModel;
    protected $logger;
    protected $cacheTypeList;

    public function __construct(
        Blog $blogModel,
        LoggerInterface $logger,
        UrlRewriteFactory $rewriteFactory,
        TypeListInterface $cacheTypeList
    ) {
        $this->_urlRewriteFactory = $rewriteFactory;
        $this->blogModel = $blogModel;
        $this->logger = $logger;
        $this->cacheTypeList = $cacheTypeList;
    }

    public function execute(Observer $observer)
    {
        // Lấy thông tin từ sự kiện
        /** @var UrlRewrite $urlRewrite */
        $urlRewrite  = $observer->getEvent()->getDataObject();
        $urlRewriteId = $urlRewrite->getData('url_rewrite');
        $id = $urlRewrite->getData('id');
        $urlRewriteModel = $this->_urlRewriteFactory->create();

        // Thiết lập store ID
        $urlRewriteModel->setStoreId(1); // Điều chỉnh store ID theo yêu cầu của bạn

        // Thiết lập system flag
        $urlRewriteModel->setIsSystem(0); // Đặt thành 0 nếu URL này không phải là URL được tạo bởi hệ thống

        // Sinh một ID path duy nhất
        $urlRewriteModel->setIdPath(rand(1, 100000)); // Bạn có thể sử dụng một ID path có ý nghĩa hơn

        // Thiết lập target path (nơi URL này sẽ chuyển hướng đến)
        $urlRewriteModel->setTargetPath("blog/blog/view/id/{$id}");

        // Thiết lập request path (đường dẫn URL thực sự bạn muốn tạo)
        $urlRewriteModel->setRequestPath("{$urlRewriteId}"); // Điều chỉnh request path theo yêu cầu

        // Lưu URL rewrite
        $urlRewriteModel->save();

        // Xóa cache
        $this->cacheTypeList->cleanType(\Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER); // Điều chỉnh loại cache cần xóa

        // Ghi log sự kiện
        $this->logger->info('Đã gọi Observer cho sự kiện Url Rewrite.');
    }
}
