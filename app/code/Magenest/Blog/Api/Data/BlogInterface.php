<?php

namespace Magenest\Blog\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface BlogInterface extends ExtensibleDataInterface
{
    const ID = 'id';
    const AUTHOR_ID = 'author_id';
    const TITLE = 'title';
    const URLREWRITE = 'url_rewrite';
    const DESCRIPTION = 'description';
    const CONTENT = 'content';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getAuthorId();

    /**
     * @param int $author_id
     * @return $this
     */
    public function setAuthorId($author_id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getUrlRewrite();

    /**
     * @param string $url_rewrite
     * @return $this
     */
    public function setUrlRewrite($url_rewrite);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string|\DateTimeInterface $created_at
     * @return $this
     */
    public function setCreatedAt($created_at);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param string|\DateTimeInterface $updated_at
     * @return $this
     */
    public function setUpdatedAt($updated_at);
}
