<?php
namespace Magenest\Blog\Api;

use Magento\Framework\DataObject;
use Magenest\Blog\Api\Data\BlogInterface;
interface BlogRepositoryInterface
{
    /**
     * Get blog by ID
     *
     * @param int $id
     * @return \Magenest\Blog\Api\Data\BlogInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param BlogInterface $blog
     * @return BlogInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(BlogInterface $blog);

    /**
     * Delete blog by ID
     *
     * @param int $id
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function deleteById($id);
}
