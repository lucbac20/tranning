<?php
namespace Magenest\Blog\Block;

use Magento\Framework\View\Element\Template;
use Magenest\Blog\Model\ResourceModel\Blog\CollectionFactory;
use Magento\Framework\App\Request\Http;

class Index extends Template
{
    protected $blogCollectionFactory;
    protected $request;

    public function __construct(
        Template\Context $context,
        CollectionFactory $blogCollectionFactory,
        Http $request,
        array $data = []
    ) {
        $this->blogCollectionFactory = $blogCollectionFactory;
        $this->request = $request;
        parent::__construct($context, $data);
    }

    public function getBlogCollection()
    {
        $blogCollection = $this->blogCollectionFactory->create();
        $blogCollection->addFieldToSelect('*');
        $blogCollection->addFieldToFilter('status', 1); // Lọc theo trạng thái active
        $blogCollection->getSelect()->joinLeft(
            ['admin_user' => $blogCollection->getTable('admin_user')],
            'main_table.author_id = admin_user.user_id',
            ['author_username' => 'admin_user.username']
        );

        return $blogCollection;
    }
}

