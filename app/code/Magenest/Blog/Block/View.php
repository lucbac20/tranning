<?php
namespace Magenest\Blog\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magenest\Blog\Model\ResourceModel\Blog\CollectionFactory;
use Magento\Framework\App\Request\Http;
use Magenest\Blog\Model\BlogFactory;
use Magento\User\Model\UserFactory; // Import UserFactory assuming it's used for admin_user

class View extends Template
{
    protected $collectionFactory;
    protected $request;
    protected $blogFactory;
    protected $userFactory;

    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        Http $request,
        BlogFactory $blogFactory,
        UserFactory $userFactory,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->request = $request;
        $this->blogFactory = $blogFactory;
        $this->userFactory = $userFactory;
        parent::__construct($context, $data);
    }
    public function getPost()
    {
        $postId = (int) $this->request->getParam('id');
        $blogPost = $this->blogFactory->create()->load($postId); // Load blog post by ID
        $user = $this->userFactory->create()->load($blogPost->getAuthorId());
        $blogPost->setData('author_username', $user->getUsername());
        return $blogPost;
    }
}
?>
