require([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data'
], function ($, authenticationPopup, customerData) {
    $(document).ready(function () {
        $('.login-authorization > a').on('click', function (event) {
            event.preventDefault();
            var customer = customerData.get('customer');
            if (!customer().firstname) {
                authenticationPopup.showModal();
                return false;
            }
            return true;
        });
    });
});
