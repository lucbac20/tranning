<?php

namespace Magenest\Movie\Block;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

class ColorPicker extends Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        $colors = $element->getEscapedValue();
        $colorsArray = !empty($colors) ? explode(',', $colors) : [];

        $html = '<div id="color-picker-container">';
        foreach ($colorsArray as $color) {
            $html .= $this->getColorPickerHtml($element, $color);
        }
        $html .= '</div>';
        $html .= '<button type="button" id="add-color-btn">Add Color</button>';
        $html .= '<input type="hidden" id="' . $element->getHtmlId() . '" name="' . $element->getName() . '" value="' . $element->getEscapedValue() . '"/>';
        $html .= '<script>
                    require(["jquery"], function ($) {
                        $(document).ready(function() {
                            function updateHiddenField() {
                                var colors = [];
                                $(".input-color-picker").each(function() {
                                    colors.push($(this).val());
                                });
                                $("#' . $element->getHtmlId() . '").val(colors.join(","));
                            }

                            function getColorPickerHtml(color = "#ffffff") {
                                return `<div class="color-picker-item">
                                            <input type="color" class="input-color-picker" value="${color}"/>
                                            <span>${color}</span>
                                            <button type="button" class="remove-color-btn">🗑️</button>
                                        </div>`;
                            }

                            $("#add-color-btn").on("click", function() {
                                $("#color-picker-container").append(getColorPickerHtml());
                            });

                            $(document).on("input", ".input-color-picker", function() {
                                $(this).next("span").text($(this).val());
                                updateHiddenField();
                            });

                            $(document).on("click", ".remove-color-btn", function() {
                                $(this).closest(".color-picker-item").remove();
                                updateHiddenField();
                            });

                            updateHiddenField();
                        });
                    });
                  </script>';
        return $html;
    }

    private function getColorPickerHtml(AbstractElement $element, $color)
    {
        return '<div class="color-picker-item">
                    <input type="color" class="input-color-picker" value="' . $color . '"/>
                    <span>' . $color . '</span>
                    <button type="button" class="remove-color-btn">🗑️</button>
                </div>';
    }
}
