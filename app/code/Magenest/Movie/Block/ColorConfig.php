<?php
namespace Magenest\Movie\Block;

use Magento\Framework\View\Element\Template;

class ColorConfig extends Template
{
    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Serialize\Serializer\Json $json,
        array $data = []
    )
    {
        $this->json = $json;
        parent::__construct($context, $data);
    }

    public function getColorGroupConfig() {
        $configData = $this->_scopeConfig
            ->getValue('magenest_hello/colors/background_colors',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $configData;
    }

}
