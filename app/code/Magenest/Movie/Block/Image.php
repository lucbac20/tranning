<?php

namespace Magenest\Movie\Block;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\ResourceConnection;

class Image extends Template
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Session
     */
    protected $customerSession;
    protected $_urlInterface;


    /**
     * Constructor
     *
     * @param Template\Context $context
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CustomerRepositoryInterface $customerRepository,
        Session $customerSession,
        ResourceConnection $resourceConnection,
        \Magento\Framework\UrlInterface $urlInterface,
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->resourceConnection = $resourceConnection;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context, $data);
    }

    /**
     * Get current customer
     *
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        return $this->customerSession->getCustomer();
    }

    /**
     * Get current customer's image
     *
     * @return string|null
     */
    public function getCustomerImage()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
            $customerImage = $customer->getCustomAttribute('file');

            if ($customerImage) {
                return $customerImage->getValue();
            }
        }
        return null;
    }
    public function getNameCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
            $customerName = $customer->getLastname();

            if ($customerName) {
                return $customerName;
            }
        }
        return null;
    }
    public function getEmailCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
            $customerImage = $customer->getEmail();

            if ($customerImage) {
                return $customerImage;
            }
        }
        return null;
    }
    public function getPhoneCustomer()
    {
        if ($this->customerSession->isLoggedIn()) {
            $customerId = $this->customerSession->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
            $connection = $this->resourceConnection->getConnection();
            $tableName = $this->resourceConnection->getTableName('customer_address_entity');

            $select = $connection->select()
                ->from($tableName, ['telephone']); // Chỉ lấy trường 'phone'

            $phoneNumbers = $connection->fetchAll($select);
            return $phoneNumbers;
        }
        return null;
    }
    public function getUrlInterfaceData()
    {
        echo $this->_urlInterface->getCurrentUrl() . '<br />';

        echo $this->_urlInterface->getUrl() . '<br />';

        echo $this->_urlInterface->getUrl('helloworld/general/enabled') . '<br />';

        echo $this->_urlInterface->getBaseUrl() . '<br />';
    }
}
