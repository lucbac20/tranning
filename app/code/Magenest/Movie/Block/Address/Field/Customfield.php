<?php
namespace Magenest\Movie\Block\Address\Field;

use Magento\Framework\View\Element\Template;

class Customfield extends Template
{
    /**
     * @var string
     */
    protected $_template = 'address/edit/field/customfield.phtml';

    /**
     * @var \Magento\Customer\Api\Data\AddressInterface
     */
    protected $_address;

    /**
     * @return string
     */
    public function getCustomfieldValue()
    {

        /** @var \Magento\Customer\Model\Data\Address $address */
        $address = $this->getAddress();
        $customfieldValue = $address->getCustomAttribute('customfield');
        if (!$customfieldValue instanceof \Magento\Framework\Api\AttributeInterface) {
            return '';
        }

        return $customfieldValue->getValue();
    }

    /**
     * Return the associated address.
     *
     * @return \Magento\Customer\Api\Data\AddressInterface
     */
    public function getAddress()
    {
        return $this->_address;
    }

    /**
     * Set the associated address.
     *
     * @param \Magento\Customer\Api\Data\AddressInterface $address
     */
    public function setAddress($address)
    {
        $this->_address = $address;
    }
}
