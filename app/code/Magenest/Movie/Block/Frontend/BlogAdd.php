<?php

namespace Magenest\Movie\Block\Frontend;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

class BlogAdd extends Template
{
    protected $userCollectionFactory;

    public function __construct(
        Context $context,
        UserCollectionFactory $userCollectionFactory,
        array $data = []
    ) {
        $this->userCollectionFactory = $userCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getAuthorOptions()
    {
        $users = $this->userCollectionFactory->create();
        $options = [];

        foreach ($users as $user) {
            $options[] = [
                'value' => $user->getId(),
                'label' => $user->getLastname()
            ];
        }

        return $options;
    }
}
