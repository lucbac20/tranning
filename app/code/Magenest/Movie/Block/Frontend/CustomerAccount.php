<?php

namespace Magenest\Movie\Block\Frontend;

use Magenest\Movie\Model\ResourceModel\Actors\CollectionFactory as ActorsCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Directors\CollectionFactory as DirectorsCollectionFactory;
use Magenest\Movie\Model\ResourceModel\Movies\CollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Element\Template;

class CustomerAccount extends Template
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var DirectorsCollectionFactory
     */
    protected $directorsCollectionFactory;
    protected $actorsCollectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     * @param DirectorsCollectionFactory $directorsCollectionFactory
     * @param Context $context
     * @param Template\Context $templateContext
     * @param array $data
     */
    public function __construct(
        CollectionFactory          $collectionFactory,
        DirectorsCollectionFactory $directorsCollectionFactory,
        ActorsCollectionFactory    $actorsCollectionFactory,
        Context                    $context,
        Template\Context           $templateContext,
        array                      $data = []

    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->directorsCollectionFactory = $directorsCollectionFactory;
        $this->actorsCollectionFactory = $actorsCollectionFactory;
        parent::__construct($templateContext, $data);
    }

    /**
     * @return array
     */
    public function getDataFromModel(): array
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function getDirectorsCollection(): array
    {
        return $this->directorsCollectionFactory->create()->getItems();
    }

    public function getActorsCollection(): array
    {
        return $this->actorsCollectionFactory->create()->getItems();
    }

    public function getDirectorNameById($directorId)
    {
        $directorsCollection = $this->directorsCollectionFactory->create();
        $director = $directorsCollection->addFieldToFilter('director_id', $directorId)->getFirstItem();
        return $director->getName();
    }

    public function getMoviesWithDirectorsAndActors()
    {
        $moviesCollection = $this->collectionFactory->create();
        $moviesCollection->getSelect()->join(['mma' => $moviesCollection->getTable('magenest_movie_actor')],
            'main_table.movie_id = mma.movie_id',
            ['movie_name' => 'main_table.name'])->join(
            ['magenest_actor' => $moviesCollection->getTable('magenest_actor')],
            'mma.actor_id = magenest_actor.actor_id', // Thay 'movie_id_column_name_here' bằng tên cột thực tế trong bảng 'magenest_actor'
            ['actor_names' => 'magenest_actor.name']
        );

        return $moviesCollection;
    }
}
