<?php
namespace Magenest\Movie\Block\Frontend;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;

class Popup extends Template
{
    protected $_customerSession;

    public function __construct(
        Template\Context $context,
        Session $customerSession,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    public function getOfferText()
    {
        $customerGroupId = $this->_customerSession->getCustomerGroupId();
        // Logic to fetch offer text based on customer group
        // Replace this with your logic to fetch the offer text based on the customer group attribute
        return "Your offer text goes here.";
    }
}
