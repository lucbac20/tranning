<?php
// File: Magenest/Blog/Block/Frontend/BlogList.php

namespace Magenest\Movie\Block\Frontend;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magenest\Movie\Model\ResourceModel\Blogs\CollectionFactory as BlogCollectionFactory;

class Blog extends Template
{
    protected $_blogCollectionFactory;

    public function __construct(
        Context $context,
        BlogCollectionFactory $blogCollectionFactory,
        array $data = []
    ) {
        $this->_blogCollectionFactory = $blogCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getBlogCollection()
    {
        $collection = $this->_blogCollectionFactory->create();
        return $collection;
    }

//    public function getViewUrl($blog)
//    {
//        return $this->getUrl('blog/index/view', ['id' => $blog->getId()]);
//    }
    public function getAddNewUrl()
    {
        return $this->getUrl('movie/blog/add');
    }
}
