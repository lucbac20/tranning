<?php
namespace Magenest\Movie\Block\Frontend;

use Magento\Catalog\Block\Product\View\AbstractView;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\ArrayUtils; // Import ArrayUtils

class DeliveryTime extends AbstractView
{
    protected $registry;

    /**
     * @var ArrayUtils
     */
    protected $arrayUtils;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Registry $registry,
        ArrayUtils $arrayUtils, // Inject ArrayUtils here
        array $data = []
    ) {
        $this->registry = $registry;
        $this->arrayUtils = $arrayUtils; // Assign ArrayUtils to class property
        parent::__construct($context, $arrayUtils, $data);
    }

    public function getProduct()
    {
        return $this->registry->registry('current_product');
    }
}
