<?php
namespace Magenest\Movie\Block\Frontend;

use Magento\Framework\View\Element\Template;
use Magento\Checkout\Model\Cart;

class DeliveryTimeCart extends Template
{
    protected $cart;

    public function __construct(Template\Context $context, Cart $cart, array $data = [])
    {
        $this->cart = $cart;
        parent::__construct($context, $data);
    }

    public function getDeliveryTime()
    {
        $quote = $this->cart->getQuote();
        return $quote->getDeliveryTime();
    }
}
