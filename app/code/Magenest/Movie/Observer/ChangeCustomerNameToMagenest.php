<?php

namespace Magenest\Movie\Observer;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Movie\Model\Movies;
use Magenest\Movie\Model\MoviesFactory;
class ChangeCustomerNameToMagenest implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $movie = $observer->getEvent()->getDataObject();
        $movie->setRating(0);
    }
}
