<?php

namespace Magenest\Movie\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class ChangeConfiguration implements ObserverInterface
{
    protected $configWriter;
    protected $request;

    public function __construct(WriterInterface $configWriter, RequestInterface $request)
    {
        $this->configWriter = $configWriter;
        $this->request = $request;
    }

    public function execute(Observer $observer)
    {
        // Retrieve configuration data from the request
        $faqParams = $this->request->getParam('groups');


        if (isset($faqParams['magenestpage']['fields']['header_title']['value'])) {
            $urlKey = $faqParams['magenestpage']['fields']['header_title']['value'];

            // Check if the value is 'ping' and change it to 'pong'
            if ($urlKey === 'ping') {
                $this->configWriter->save('magenest_hello/magenestpage/header_title', 'pong');
            } else {
                // Save the actual value if it's not 'ping'
                $this->configWriter->save('magenest_hello/magenestpage/header_title', $urlKey);
            }
        }

        return $this;
    }
}
