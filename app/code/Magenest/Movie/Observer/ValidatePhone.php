<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class ValidatePhone implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $customer = $observer->getEvent()->getDataObject();
//        print_r($customer);
//        exit();
        $telephone = $customer->getPhoneNumber();

        if (preg_match('/^\+84/', $telephone)) {
            $telephone = preg_replace('/^\+84/', '0', $telephone);
        }

        if (!preg_match('/^0\d{9}$/', $telephone)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The telephone number is invalid. It must start with 0 or +84 and be exactly 10 digits long.')
            );
        }

        $customer->setPhoneNumber($telephone);
    }
}
