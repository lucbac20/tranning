require([
    'jquery'
], function ($) {
    $(document).ready(function(){
        $('[name="phone_number"]').val('123');
        $('input[name="phone_number"]').on('input', function(){
            var value = $(this).val();
            // Nếu ký tự đầu tiên không phải là '0', thêm '0' vào đầu
            if(value.length > 0 && value.charAt(0) !== '0') {
                value = '0' + value;
            }

            // Gán giá trị kết quả cho ô input 'phone_number'
            $('input[name="phone_number"]').val(value);
        });
    });
});
