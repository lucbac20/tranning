<?php
namespace Magenest\Movie\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;

class CustomSection extends AbstractModifier
{
    public function modifyMeta(array $meta)
    {
        $meta['custom_section'] = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Custom Section'),
                        'componentType' => 'fieldset',
                        'collapsible' => true,
                        'sortOrder' => 100,
                    ],
                ],
            ],
            'children' => [
                'custom_start_date' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Start Date'),
                                'componentType' => 'field',
                                'formElement' => 'date',
                                'dataScope' => 'custom_start_date',
                                'validation' => [
                                    'required-entry' => true,
                                ],
                                'sortOrder' => 10,
                            ],
                        ],
                    ],
                ],
                'custom_end_date' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('End Date'),
                                'componentType' => 'field',
                                'formElement' => 'date',
                                'dataScope' => 'custom_end_date',
                                'validation' => [
                                    'required-entry' => true,
                                ],
                                'sortOrder' => 20,
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $meta;
    }

    public function modifyData(array $data)
    {
        return $data;
    }
}
