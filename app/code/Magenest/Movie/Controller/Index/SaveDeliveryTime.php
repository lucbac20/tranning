<?php
namespace Magenest\Movie\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Action;
use Magento\Quote\Model\QuoteRepository;

class SaveDeliveryTime extends Action
{
    protected $cart;
    protected $quoteRepository;

    public function __construct(Context $context, Cart $cart, QuoteRepository $quoteRepository)
    {
        $this->cart = $cart;
        $this->quoteRepository = $quoteRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $deliveryTime = $this->getRequest()->getParam('delivery_time');
        $customDeliveryDate = $this->getRequest()->getParam('custom_delivery_date');

        if ($deliveryTime == 'choose_date') {
            $deliveryTime = $customDeliveryDate;
        }

        $quote = $this->cart->getQuote();
        $quote->setDeliveryTime($deliveryTime);
        $this->quoteRepository->save($quote);

        $this->_redirect('checkout/cart');
    }
}
