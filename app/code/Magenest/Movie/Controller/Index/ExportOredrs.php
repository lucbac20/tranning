<?php
namespace Magenest\Movie\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class ExportOrders extends Action
{
    protected $fileFactory;
    protected $orderRepository;
    protected $searchCriteriaBuilder;
    protected $_filesystem;

    public function __construct(
        Context $context,
        FileFactory $fileFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Filesystem $filesystem
    )
    {
        $this->fileFactory = $fileFactory;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_filesystem = $filesystem;
        parent::__construct($context);
    }

    public function execute()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $orders = $this->orderRepository->getList($searchCriteria)->getItems();
        $csvData = [];
        $csvData[] = ['Order ID', 'Customer Email', 'Order Total'];
        foreach ($orders as $order) {
            $csvData[] = [
                $order->getEntityId(),
                $order->getCustomerEmail(),
                $order->getGrandTotal()
            ];
        }

        $fileName = 'orders.csv';
        $filePath = 'export/' . $fileName;

        $directory = $this->_filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $stream = $directory->openFile($filePath, 'w+');

        foreach ($csvData as $rowData) {
            $stream->writeCsv($rowData);
        }

        $stream->close();

        $content = [
            'type' => 'filename',
            'value' => $filePath,
            'rm' => true
        ];

        $response = $this->fileFactory->create($fileName, $content, DirectoryList::VAR_DIR);
        return $response;
    }
}
