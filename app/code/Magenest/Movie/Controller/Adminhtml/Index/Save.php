<?php

namespace Magenest\Movie\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Movie\Model\MoviesFactory;
use Magenest\Movie\Model\ResourceModel\Movies as MoviesResource;


class Save extends Action
{
    protected $movieFactory;
    protected $movieResource;

    public function __construct(
        Context $context,
        MoviesFactory $movieFactory,
        MoviesResource $movieResource
    )
    {
        $this->movieFactory = $movieFactory;
        $this->movieResource = $movieResource;
        parent::__construct($context);
    }

    public function execute()
    {
        // Check if the form data is available
        $data = $this->getRequest()->getParams();

        if (!$data) {
            $this->messageManager->addError(__('Invalid data.'));
            return $this->_redirect('*/*/index');
        }
        try {
            // Create a new instance of movie model
            $movie = $this->movieFactory->create();

            // Set data to movie model
            $movie->setData($data);

            // Save movie data
            $this->movieResource->save($movie);

            $this->messageManager->addSuccess(__('Movie has been saved successfully.'));

            // Redirect to listing page
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return $this->_redirect('*/*/index');
        }
    }
}
