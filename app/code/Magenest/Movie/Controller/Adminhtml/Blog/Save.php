<?php

namespace Magenest\Movie\Controller\Adminhtml\Blog;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Movie\Model\BlogFactory;
use Magenest\Movie\Model\ResourceModel\Blog as BlogResource;
use Magenest\Movie\Model\ResourceModel\Blogs\CollectionFactory as BlogCollectionFactory;

class Save extends Action
{
    protected $blogFactory;
    protected $blogResource;
    protected $blogCollectionFactory;
    protected $resultFactory;

    public function __construct(
        Context $context,
        BlogCollectionFactory $blogCollectionFactory,
        BlogFactory $blogFactory,
        BlogResource $blogResource,
        ResultFactory $resultFactory
    )
    {
        $this->blogCollectionFactory = $blogCollectionFactory;
        $this->blogFactory = $blogFactory;
        $this->blogResource = $blogResource;
        $this->resultFactory = $resultFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        // Check if the form data is available
        $data = $this->getRequest()->getPostValue();

        if (!$data) {
            $this->messageManager->addError(__('Invalid data.'));
            return $this->_redirect('*/*/index');
        }

        try {
            // Check if the URL Rewrite already exists
            $existingBlog = $this->blogCollectionFactory->create()
                ->addFieldToFilter('url_rewrite', $data['url_rewrite'])
                ->getFirstItem();

            if ($existingBlog->getId()) {
                // If URL Rewrite already exists, show error message
                $this->messageManager->addError(__('URL Rewrite already exists. Please choose a different URL Rewrite.'));
                return $this->_redirect('*/*/index');
            }

            // Create a new instance of blog model
            $blog = $this->blogFactory->create();

            // Set data to blog model
            $blog->setData($data);

            // Save blog data
            $this->blogResource->save($blog);

            $this->messageManager->addSuccess(__('Blog has been saved successfully.'));

            // Redirect to listing page
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/index');

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return $this->_redirect('*/*/index');
        }
    }
}
