<?php
namespace Magenest\Movie\Plugin;

use Magento\Customer\Api\AddressRepositoryInterface as Subject;
use Magento\Customer\Api\Data\AddressInterface as Entity;

/**
 * Class AddNoteFieldToAddressEntity
 *
 * @package Yireo\ExampleAddressFieldNote\Plugin
 */
class AddExtraFieldToAddressEntity
{
    protected $httpRequest;
    protected $logger;

    public function __construct(
        \Magento\Framework\App\RequestInterface $httpRequest,
        \Magento\Framework\Logger\Monolog $logger
    )
    {
        $this->httpRequest = $httpRequest;
        $this->logger = $logger;
    }

    /**
     * @param Subject $subject
     * @param Entity $entity
     *
     * @return Entity
     */
    public function afterGetById(Subject $subject, Entity $entity)
    {
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            return $entity;
        }

        $customfield = $this->getCustomfieldByEntityId($entity);
        $extensionAttributes->setCustomfield($customfield);
        $entity->setExtensionAttributes($extensionAttributes);

        return $entity;
    }

    /**
     * @param Subject $subject
     * @param Entity $entity
     *
     * @return [Entity]
     */
    public function beforeSave(Subject $subject, Entity $entity)
    {
        $extensionAttributes = $entity->getExtensionAttributes();
        if ($extensionAttributes === null) {
            return [$entity];
        }

        // @todo: Really dirty hack, because Magento\Customer\Controller\Address\FormPost does not support Extension Attributes
        $customfield = $this->httpRequest->getParam('customfield');
        $entity->setCustomAttribute('customfield', $customfield);

        return [$entity];
    }

    /**
     * @param Entity $entity
     *
     * @return string
     */
    private function getCustomfieldByEntityId(Entity $entity)
    {
        $attribute = $entity->getCustomAttribute('customfield');
        if ($attribute) {
            return $attribute->getValue();
        }

        return '';
    }
}
