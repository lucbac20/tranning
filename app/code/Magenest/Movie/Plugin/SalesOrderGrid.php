<?php

namespace Magenest\Movie\Plugin;

use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;
use Magento\Framework\Api\Filter;

class SalesOrderGrid
{
    public function aroundGetReport(CollectionFactory $subject, callable $proceed, $requestName)
    {
        $collection = $proceed($requestName);

        if ($requestName === 'sales_order_grid_data_source') {
            $collection->getSelect()->columns(
                ['odd_even' => new \Zend_Db_Expr("IF(main_table.entity_id % 2 = 0, 'Even', 'Odd')")]
            );
        }

        return $collection;
    }
}
