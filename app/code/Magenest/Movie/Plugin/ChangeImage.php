<?php

namespace Magenest\Movie\Plugin;

use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class ChangeImage
{
    protected $productRepository;
    protected $imageHelper;

    public function __construct(
        ProductRepository $productRepository,
        ImageHelper $imageHelper
    ) {
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
    }

    public function afterGetItemData($subject, $result, $item)
    {
        try {
            // Lấy ID sản phẩm từ kết quả
            $productId = $result['product_id'];

            // Lấy đối tượng sản phẩm từ ProductRepository
            $product = $this->productRepository->getById($productId);

            // Nếu sản phẩm là sản phẩm có cấu hình (configurable product)
            if ($product->getTypeId() == Configurable::TYPE_CODE) {
                // Lấy ID sản phẩm con được chọn
                $childProductId = $item->getChildren()[0]->getProductId();
                $childProduct = $this->productRepository->getById($childProductId);

                // Lấy URL ảnh của sản phẩm con
                $imageUrl = $this->imageHelper->init($childProduct, 'product_base_image')->getUrl();
                $childProduct = $this->productRepository->getById($childProductId);
                $childProductName = $childProduct->getName();
                $result['product_name']=$childProductName;
                $result['product_image']['src'] = $imageUrl;
            } else {
                // Lấy URL ảnh sản phẩm (thumbnail hoặc base image)
                $imageUrl = $this->imageHelper->init($product, 'product_base_image')->getUrl();
                $result['product_image']['src'] = $imageUrl;
            }
        } catch (NoSuchEntityException $e) {
            // Xử lý ngoại lệ nếu sản phẩm không tồn tại
            $result['product_image']['src'] = 'https://letsenhance.io/static/8f5e523ee6b2479e26ecc91b9c25261e/1015f/MainAfter.jpg';
        }

        return $result;
    }
}
