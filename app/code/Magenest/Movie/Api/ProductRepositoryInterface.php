<?php
namespace Magenest\Movie\Api;
interface ProductRepositoryInterface
{
    /**
     * Return a filtered product.
     *
     * @param int $id
     * @return \Magenest\Movie\Api\ResponseItemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getItem(int $id);
    /**
     * Set descriptions for the products.
     *
     * @param \Magenest\Movie\Api\RequestItemInterface[] $products
     * @return void
     */
    public function setDescription(array $products);
}
