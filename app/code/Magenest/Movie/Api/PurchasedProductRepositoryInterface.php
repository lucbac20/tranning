<?php

namespace Magenest\Movie\Api;

interface PurchasedProductRepositoryInterface
{
    /**
     * Get all purchased products
     *
     * @return \Magento\Movie\Api\Data\OrderItemInterface[]
     */
    public function getAllPurchasedProducts();
}
