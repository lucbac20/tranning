<?php
namespace Magenest\Movie\Model;
class Movies extends \Magento\Framework\Model\AbstractModel{
    protected $_eventPrefix='magenest_movies';
    public function setRating($rating)
    {
        return $this->setData('rating', $rating);
    }

    public function getRating()
    {
        return $this->getData('rating');
    }
    protected function _construct(){
        $this->_init('Magenest\Movie\Model\ResourceModel\Movies');
    }

}

