<?php
namespace Magenest\Movie\Model\Config\Source;

use Magento\Framework\Data\Form\Element\AbstractElement;

class ReloadButton extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * Return element HTML
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $html = $element->getElementHtml();
        $html .= '<button id="button_id" type="button">' . __('Button') . '</button>';
        $html .= '<script>
            require(["jquery"], function($){
                $(document).ready(function(){
                    $("#button_id").click(function(){
                        location.reload();
                    });
                });
            });
            </script>';
        return $html;
    }
}
