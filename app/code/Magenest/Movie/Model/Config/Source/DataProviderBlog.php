<?php
namespace Magenest\Movie\Model\Config\Source;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magenest\Movie\Model\ResourceModel\Blogs\CollectionFactory;

class DataProviderBlog extends AbstractDataProvider
{
    protected $_loadedData;
    protected $collection;
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
}

