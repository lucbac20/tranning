<?php
namespace Magenest\Movie\Model\Config\Source;

use Magenest\Movie\Model\ResourceModel\Movies\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;


class RowMovieOptions extends \Magento\Config\Block\System\Config\Form\Field {
    protected $collectionFactory;
    protected $scopeConfig;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,CollectionFactory $collectionFactory, ScopeConfigInterface $scopeConfig,array $data = []
    ) {
        parent::__construct($context, $data);

        $this->collectionFactory = $collectionFactory;
        $this->scopeConfig= $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element) {
        $html = $element->getElementHtml();

        // Get collection
        $collection = $this->collectionFactory->create();

        // Count rows
        $count = $collection->getSize();
        $element->setValue($count);

        $element->setReadonly(true);


        return $element->getElementHtml();
    }

}
