<?php

namespace Magenest\Movie\Model\Config\Source;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class OddEven extends Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $h1 = '<h1>EVEN</h1>';
        $h2 = '<h1>EVEN</h1>';
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')] = $item['entity_id'] % 2 === 0 ? '<span class="grid-severity-notice">Even</span>' : '<span class="grid-severity-critical">Odd</span>';
            }
        }

        return $dataSource;
    }
}
