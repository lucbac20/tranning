<?php

namespace Magenest\Movie\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magenest\Movie\Model\ResourceModel\Directors\CollectionFactory as DirectorCollectionFactory;

class Director implements OptionSourceInterface
{
    /**
     * @var DirectorCollectionFactory
     */
    protected $directorCollectionFactory;

    /**
     * Constructor
     *
     * @param DirectorCollectionFactory $directorCollectionFactory
     */
    public function __construct(DirectorCollectionFactory $directorCollectionFactory)
    {
        $this->directorCollectionFactory = $directorCollectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $directors = $this->directorCollectionFactory->create()->getItems();
        $options = [];

        foreach ($directors as $director) {
            $options[] = ['value' => $director->getId(), 'label' => $director->getName()];
        }

        return $options;
    }
}
