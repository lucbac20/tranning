<?php

namespace Magenest\Movie\Model\Config\Source;

use Magento\Ui\Component\Listing\Columns\Column;

class StarRating extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $rating = $item['rating'];
                $stars = $this->convertToStars($rating);
                $item[$this->getData('name')] = $stars;
            }
        }

        return $dataSource;
    }

    private function convertToStars($rating)
    {
        $stars = '';
        if ($rating >= 0 && $rating <= 5) {
            for ($i = 0; $i < $rating; $i++) {
                $stars .= '★';
            }
            for ($i = $rating; $i < 5; $i++) {
                $stars .= '☆';
            }
        }
        return $stars;
    }
}
