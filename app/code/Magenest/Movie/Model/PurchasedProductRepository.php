<?php

namespace Magenest\Movie\Model;

use Magenest\Movie\Api\PurchasedProductRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class PurchasedProductRepository implements PurchasedProductRepositoryInterface
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getAllPurchasedProducts()
    {
        $searchCriteria = $this->orderRepository->getSearchCriteriaBuilder()->create();
        $orders = $this->orderRepository->getList($searchCriteria);

        $purchasedProducts = [];
        foreach ($orders->getItems() as $order) {
            foreach ($order->getAllVisibleItems() as $item) {
                $purchasedProducts[] = $item;
            }
        }

        return $purchasedProducts;
    }
}
