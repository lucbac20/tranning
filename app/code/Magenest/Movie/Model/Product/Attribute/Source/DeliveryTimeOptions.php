<?php
namespace Magenest\Movie\Model\Product\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class DeliveryTimeOptions extends AbstractSource
{
    public function getAllOptions()
    {
        return [
            ['value' => 1, 'label' => __('Same Day Delivery')],
            ['value' => 2, 'label' => __('Customer Chooses Delivery Date')],
        ];
    }
}
