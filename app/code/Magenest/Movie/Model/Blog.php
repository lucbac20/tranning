<?php
namespace Magenest\Movie\Model;

use Magento\Framework\Model\AbstractModel;

class Blog extends AbstractModel
{
    protected $_eventPrefix = 'magenest_blog';

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\ResourceModel\Blog');
    }
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultCacheTags()
    {
        return [self::CACHE_TAG];
    }

}
