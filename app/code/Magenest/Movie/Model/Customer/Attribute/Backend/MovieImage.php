<?php

namespace Magenest\Movie\Model\Customer\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\Exception\LocalizedException;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class MovieImage extends AbstractBackend
{
    protected $uploaderFactory;
    protected $filesystem;

    public function __construct(
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem
    ) {
        $this->uploaderFactory = $uploaderFactory;
        $this->filesystem = $filesystem;
    }

    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $value = $object->getData($attributeCode);

        if (isset($_FILES[$attributeCode]['name']) && $_FILES[$attributeCode]['name'] != '') {
            try {
                $uploader = $this->uploaderFactory->create(['fileId' => $attributeCode]);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                $destinationPath = $mediaDirectory->getAbsolutePath('custom_folder'); // Custom folder path

                $result = $uploader->save($destinationPath);
                if ($result['file']) {
                    $object->setData($attributeCode, 'custom_folder/' . $result['file']);
                }
            } catch (\Exception $e) {
                throw new LocalizedException(__('File upload error.'));
            }
        } else {
            $object->setData($attributeCode, $value);
        }

        return parent::beforeSave($object);
    }
}
