<?php

namespace Magenest\Movie\Model\Renderer;

use Magento\Framework\DataObject;

class OddEven extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    public function getOptionArray()
    {
        return [
            'even' => '<p class="admin__field-success-text">Even</p>',
            'odd' => '<p class="admin__field-label">Odd</p>'
        ];
    }

    public function toOptionArray()
    {
        return $this->getOptionArray();
    }
}
