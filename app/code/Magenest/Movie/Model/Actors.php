<?php
namespace Magenest\Movie\Model;
class Actors extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
    protected function _construct(){
        $this->_init('Magenest\Movie\Model\ResourceModel\Actors');
    }
    const CACHE_TAG = 'amir_prince_mymodel';
    protected $_cacheTag = 'amir_prince_mymodel';
    protected $_eventPrefix = 'amir_prince_mymodel';

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }
}
