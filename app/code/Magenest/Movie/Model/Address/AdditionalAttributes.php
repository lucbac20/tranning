<?php
namespace Magenest\Movie\Model\Address;

use Magento\Framework\Api\AbstractSimpleObject;

class AdditionalAttributes extends AbstractSimpleObject implements \Magento\Customer\Api\Data\AddressExtensionInterface
{
    /**
     * @param string $note
     * @return void
     */
    public function setCustomfield($customfield)
    {
        $this->setData('customfield', $customfield);
    }

    /**
     * @return mixed|null
     */
    public function getCustomfield()
    {
        return $this->_get('customfield');
    }
}
