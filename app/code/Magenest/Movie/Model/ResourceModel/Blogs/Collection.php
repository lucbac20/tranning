<?php

namespace Magenest\Movie\Model\ResourceModel\Blogs;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'entity_id'; // Primary key field name of main table

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\Blog\Model\Order', // Model class
            'Magenest\Blog\Model\ResourceModel\Order' // Resource Model class
        );
    }

    /**
     * Initialization here if needed
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->join(
            ['customer_entity' => $this->getTable('customer_entity')], // Table to join with alias
            'main_table.customer_id = customer_entity.entity_id', // Join condition
            ['customer_email' => 'customer_entity.email'] // Columns to select
        );
        return $this;
    }
}
