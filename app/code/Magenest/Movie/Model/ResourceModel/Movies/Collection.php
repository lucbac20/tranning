<?php

namespace Magenest\Movie\Model\ResourceModel\Movies;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Psr\Log\LoggerInterface;

class Collection extends AbstractCollection implements SearchResultInterface
{
    protected $_idFieldName = 'movie_id';
    protected $_eventPrefix = 'magenest_movie_collection';
    protected $_eventObject = 'movie_collection';

    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    protected function _construct()
    {
        $this->_init('Magenest\Movie\Model\Movies', 'Magenest\Movie\Model\ResourceModel\Movies');
    }

    // Implement SearchResultInterface methods
    public function getItems()
    {
        return parent::getItems();  // Gọi phương thức getItems của lớp cha
    }

    public function setItems(array $items = null)
    {
        $this->_setIsLoaded(true);
        $this->_items = $items;
        return $this;
    }

    public function getSearchCriteria()
    {
        return null;
    }

    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    public function setAggregations($aggregations)
    {
        return $this;
    }

    public function getAggregations()
    {
        return null;
    }
}
