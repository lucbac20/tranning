<?php
namespace Magenest\Movie\Model\ResourceModel\Movies_Actors;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends \Magento\Framework\Data\Collection
{
    protected function _construct()
    {
        $this->_init(
            'Magenest\Movie\Model\Movies',
            'Magenest\Movie\Model\ResourceModel\Movies'
        );
    }
}
